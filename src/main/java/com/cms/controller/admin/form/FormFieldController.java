package com.cms.controller.admin.form;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cms.Feedback;
import com.cms.controller.admin.BaseController;
import com.cms.entity.Form;
import com.cms.entity.FormField;
import com.cms.routes.RouteMapping;
import com.cms.util.FieldUtils;


/**
 * Controller - 表单字段
 * 
 * 
 * 
 */
@RouteMapping(url = "/admin/form/form_field")
public class FormFieldController extends BaseController {
	
	/**
	 * 列表
	 */
	public void index() {
		setListQuery();
		Integer formId = getParaToInt("formId");
		List<FormField> formFields = new FormField().dao().findList(formId);
		setAttr("formFields", formFields);
		setAttr("formId", formId);
		render(getView("form/form_field/index"));
	}
    
    /**
     * 检查名称是否存在
     */
    public void checkName() {
        Integer formId = getParaToInt("formId");
        String name = getPara("name");
        if (StringUtils.isEmpty(name)) {
            renderJson(false);
            return;
        }
        renderJson(!new FormField().dao().nameExists(formId,name));
    }


	/**
	 * 添加
	 */
	public void add() {
		Integer formId = getParaToInt("formId");
		setAttr("form", new Form().dao().findById(formId));
		render(getView("form/form_field/add"));
	}

	/**
	 * 保存
	 */
	public void save() {
		FormField formField = getModel(FormField.class,"",true); 
		formField.setCreateDate(new Date());
		formField.setUpdateDate(new Date());
		formField.save();
		//数据库表
		Form form = formField.getForm();
	    FieldUtils.add(form.getTableName(), formField.getName());
		redirect(getListQuery("/admin/form/form_field?formId="+form.getId()));
	}

	
	/**
	 * 编辑
	 */
	public void edit() {
		Integer id = getParaToInt("id");
		setAttr("formField", new FormField().dao().findById(id));
		render(getView("form/form_field/edit"));
	}

	/**
	 * 修改
	 */
	public void update() {
		FormField formField = getModel(FormField.class,"",true); 
		FormField pformField = new FormField().dao().findById(formField.getId());
		Form form = pformField.getForm();
		String tableName = form.getTableName();
		String oldFieldName = pformField.getName();
		String newFieldName = formField.getName();
		formField.setUpdateDate(new Date());
		formField.update();
		//数据库表
        FieldUtils.update(tableName, oldFieldName,newFieldName);
		redirect(getListQuery("/admin/form/form_field?formId="+formField.getFormId()));
	}
	
   /**
     * 修改排序
     */
    public void updateSort(){
        String sortArray = getPara("sortArray");
        JSONArray jsonArray = JSONArray.parseArray(sortArray);
        for(int i=0;i<jsonArray.size();i++){
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Long id = jsonObject.getLong("id");
            Integer sort = jsonObject.getInteger("sort");
            FormField formField = new FormField().dao().findById(id);
            formField.setSort(sort);
            formField.update();
        }
        renderJson(Feedback.success(new HashMap<>()));
    }

	/**
	 * 删除
	 */
	public void delete() {
		Integer ids[] = getParaValuesToInt("ids");
		if(ArrayUtils.isNotEmpty(ids)){
			for(Integer id:ids){
				FormField formField = new FormField().dao().findById(id);
				Form form = formField.getForm();
				//数据库表
	            FieldUtils.delete(form.getTableName(), formField.getName());
	            formField.delete();
			}
		}
		renderJson(Feedback.success(new HashMap<>()));
	}

}